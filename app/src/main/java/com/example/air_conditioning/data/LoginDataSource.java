package com.example.air_conditioning.data;

import com.example.air_conditioning.data.model.LoggedInUser;


import com.example.air_conditioning.ui.login.LoginActivity;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {


    public Result<LoggedInUser> login(String username, String password) {

        try {

            // TODO: handle loggedInUser authentication


            LoggedInUser fakeUser = new LoggedInUser(java.util.UUID.randomUUID().toString(), LoginActivity.name);
           // System.out.println("name in logged in: " + LoginActivity.name);
            //LoggedInUserView fakeUser = new LoggedInUserView(LoginActivity.name);
            return new Result.Success<>(fakeUser);
        } catch (Exception e) {
            return new Result.Error(new IOException("Error logging in", e));
        }
    }

    public void logout() {
        // TODO: revoke authentication
    }
}