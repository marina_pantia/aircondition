package com.example.air_conditioning.ui.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.example.air_conditioning.R;

import java.util.ArrayList;

public class ModeSpinnerAdapter extends BaseAdapter {

    Context context;
    LayoutInflater inflter;
    ArrayList<Integer> imagesMode;

    public ModeSpinnerAdapter(Context context, ArrayList<Integer> imagesMode){
        this.context = context;
        this.imagesMode = imagesMode;
        inflter = (LayoutInflater.from(context));
    }
    @Override
    public int getCount() {
        return imagesMode.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.custom_spinner_items, null);
        ImageView icon = (ImageView) convertView.findViewById(R.id.imageView);
        icon.setImageResource(imagesMode.get(position));
        return convertView;
    }
}
