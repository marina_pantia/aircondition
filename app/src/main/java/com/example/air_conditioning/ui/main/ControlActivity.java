package com.example.air_conditioning.ui.main;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.air_conditioning.R;
import com.example.air_conditioning.ui.devices.DeviceSelectActivity;
import com.example.air_conditioning.ui.login.LoginActivity;
import com.example.air_conditioning.ui.manual.AdvancedManual;
import com.example.air_conditioning.ui.manual.ControlManual;
import com.example.air_conditioning.ui.manual.PresetManual;
import com.example.air_conditioning.ui.presets.PresetActivity;
import com.example.air_conditioning.ui.presets.PresetListItem;

import org.w3c.dom.Text;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ControlActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    TextView celcius;
    TextView swivel;
    TextView mode;
    TextView volumeOfFan;
    Button eco;
    Button save_preset;

    TextView turnOff;
    TextView turnOn;
    TextView repeat;
    TextView deviceName;
    Button plus;
    Button minus;
    Spinner modeList;
    Button on_off ;
    Button advancedBtn;
    Boolean advanced = false;
    Spinner fanVolume;
    Spinner swivelSel;

    Button timerOff;
    Button timerOn;
    Button repeatButton;


    static String device = "";
    String presetName = "";
    int temperature =0;
    int imageMode ;
    int imageFan;
    boolean sw_on_off = false;
    ArrayList<Integer> modeListOfimages;
    ArrayList<Integer> FanListOfimages;
    ArrayList<String> swivelList;
    static int modeSelect = 0; // variable for selecting a mode image in method itemSelectListener
    static int fanVolumeSelect = 0; // variable for selecting a fan volume image in method itemSelectListener
    static int clickEco = 0;
    static boolean ecoIsClicked = false;
    static boolean sw_high_temp = false;
    static boolean sw_low_temp = false;



    private void Logout() {
        LoginActivity login = new LoginActivity();
        if (!login.getSkipped()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(ControlActivity.this);
            builder.setTitle("Μήνυμα επιβεβαίωσης");
            builder.setMessage("Είστε σίγουροι ότι θέλετε να αποσυνδεθείτε από τον λογαριασμό σας;");

            builder.setCancelable(true);
            builder.setPositiveButton("ΝΑΙ", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();
                    Intent intent = new Intent(ControlActivity.this, LoginActivity.class);
                    startActivity(intent);
                    return;

                }
            });
            builder.setNegativeButton("ΟΧΙ",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            return;
                        }
                    });
            builder.show();
        } else {
            Toast.makeText(ControlActivity.this, "Δεν έχετε συνδεθεί σε κάποιο λογαριασμό για να αποσυνδεθείτε", Toast.LENGTH_LONG).show();
        }
    }

    private void Manual() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(ControlActivity.this);
        builder.setTitle("Εγχειρίδιο");
        builder.setMessage("Ενότητες: ");

        final ListView unitList = new ListView(ControlActivity.this);

        ArrayList<String> listunit = new ArrayList<>();
        final String unitPresets = "Ενότητα 1η: Έτοιμα Προγραμμάτα";
        final String unitControl = "Ενότητα 2η: Τηλεχειριστήριο και εικονίδια";
        final String unitAdvanced = "Ενότητα 3η: Λειτουργία για προχωρημένους";

        listunit.add(unitPresets);
        listunit.add(unitControl);
        listunit.add(unitAdvanced);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ControlActivity.this, android.R.layout.simple_list_item_1, android.R.id.text1, listunit);

        unitList.setAdapter(adapter);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        unitList.setLayoutParams(lp);
        builder.setView(unitList);
        builder.setCancelable(true);
        builder.setPositiveButton("ΟΚ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                return;

            }
        });

        builder.show();

        unitList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = (String) unitList.getItemAtPosition(position);
                if(item.equals(unitPresets)) {
                    Intent intent1 = new Intent(ControlActivity.this, PresetManual.class);
                    startActivity(intent1);
                }
                else if(item.equals(unitControl)) {
                    Intent intent2 = new Intent(ControlActivity.this, ControlManual.class);
                    startActivity(intent2);
                }
                else {
                    Intent intent3 = new Intent(ControlActivity.this, AdvancedManual.class);
                    startActivity(intent3);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.manual: {
                Manual();
                System.err.println("Manual");
                break;
            }
            case R.id.logout: {
                Logout();
                break;
            }
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control);

        celcius = (TextView) findViewById(R.id.control_temperature_value_tv);

        timerOff = (Button)findViewById(R.id.btn_turnOff);
        timerOn = (Button)findViewById(R.id.btn_turnOn);
        repeatButton = (Button)findViewById(R.id.btn_repeat);
        turnOff = (TextView)findViewById(R.id.tv_turnOff);
        turnOn = (TextView)findViewById(R.id.tv_turnOn);
        repeat = (TextView)findViewById(R.id.tv_repeat);
        deviceName = (TextView)findViewById(R.id.control_device_name_tview);
        plus = (Button)findViewById(R.id.control_temperature_plus_img_btn);
        minus = (Button)findViewById(R.id.control_temperature_minus_img_btn);
        on_off = (Button)findViewById(R.id.control_turn_on_off_btn);
        modeList = (Spinner)findViewById(R.id.control_temp_mode_spnr);
        advancedBtn = (Button) findViewById(R.id.control_advanced_btn);
        fanVolume = (Spinner)findViewById(R.id.control_fan_speed_spnr);
        swivel =  (TextView) findViewById(R.id.tv_fanSwivel);
        mode =  (TextView) findViewById(R.id.tv_mode);;
        volumeOfFan = (TextView) findViewById(R.id.tv_fanSpeed);;
        eco = (Button)findViewById(R.id.control_eco_btn);;
        save_preset = (Button)findViewById(R.id.control_apply_changes_btn);;
        swivelSel = (Spinner) findViewById(R.id.control_fan_swivel_spnr);

        turnOff.setVisibility(View.GONE);
        turnOn.setVisibility(View.GONE);
        repeat.setVisibility(View.GONE);
        timerOff.setVisibility(View.GONE);
        timerOff.setVisibility(View.GONE);
        repeatButton.setVisibility(View.GONE);

        modeListOfimages = new ArrayList<>();
        FanListOfimages = new ArrayList<>();


        Intent intent = getIntent();
        final HashMap<String, PresetListItem> selected_preset = (HashMap<String, PresetListItem>) intent.getSerializableExtra("message");

        save_preset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savePreset(v, selected_preset);

            }
        });

        eco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickEco++;
                if(clickEco%2!=0) {
                    ecoIsClicked = true;

                    new AlertDialog.Builder(ControlActivity.this)
                            .setTitle("Οικολογική λειτουργία")
                            .setMessage("Είστε σίγουροι ότι θέλετε να ενεργοποιήσετε την λειτουργία ECO; Ενεργοποιώντας την οικολογική λειτουργία, τα επίπεδα θερμοκρασίας και η δύναμη ανεμιστήρα θα προσαρμοστούν και δεν θα μπορείτε να τα αλλάξετε μέχρι να την απενεργοποιήσετε (Για περισσότερες πληροφορίες σχετικά με την λειτουργία ECO μεταβείτε στο μενού πληροφορίες -> Ενότητα 2η: Τηλεχειριστήριο και εικονίδια")
                            .setPositiveButton("ΝΑΙ", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    temperature = 24;
                                    fanVolumeSelect = R.drawable.fan_volume1_50;
                                    if(FanListOfimages!=null) {
                                        for (int i = 0; i < FanListOfimages.size(); i++) {
                                            if (fanVolumeSelect == FanListOfimages.get(i)) {
                                                fanVolume.setSelection(i);
                                            }
                                        }
                                    }
                                    Toast.makeText(ControlActivity.this, "Η λειτουργία ECO ενεργοποιήθηκε. Για να την απενεργοποιήσετε κάνετε κλικ στο κουμπί ECO", Toast.LENGTH_LONG).show();
                                    fanVolume.setEnabled(false);
                                    plus.setEnabled(false);
                                    minus.setEnabled(false);
                                    celcius.setText(Integer.toString(temperature) + "°C");
                                    eco.setBackgroundColor(Color.parseColor("#B9F6CA"));

                                }
                            })

                            // A null listener allows the button to dismiss the dialog and take no further action.
                            .setNegativeButton("ΑΚΥΡΩΣΗ", null)
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .show();
                }
                else{
                    ecoIsClicked = false;
                    fanVolume.setEnabled(true);
                    plus.setEnabled(true);
                    minus.setEnabled(true);
                    eco.setBackgroundColor(Color.parseColor("#FFFFFF"));
                }
            }
        });


        for(Map.Entry<String, PresetListItem> entry : selected_preset.entrySet()){
            Toast.makeText(ControlActivity.this, "device selected: " + entry.getKey(), Toast.LENGTH_SHORT).show();
            device = entry.getKey();
            presetName = entry.getValue().getmImageNames();
            temperature = Integer.parseInt(entry.getValue().getTemperature().substring(0,entry.getValue().getTemperature().length()-2));
            imageMode = entry.getValue().getImages();
            imageFan = entry.getValue().getFanVolume();
        }

        modeListOfimages.add(R.drawable.snowflake_50);
        modeListOfimages.add(R.drawable.sun_50);
        modeListOfimages.add(R.drawable.rain_50);
        ModeSpinnerAdapter spinnerAdapter=new ModeSpinnerAdapter(getApplicationContext(),modeListOfimages);
        modeList.setAdapter(spinnerAdapter);
        modeList.setOnItemSelectedListener(this);
        for(int i = 0 ; i<modeListOfimages.size();i++){
            if(imageMode==modeListOfimages.get(i)){
                modeSelect = modeListOfimages.get(i);
                modeList.setSelection(i);
            }
        }
        FanListOfimages.add(R.drawable.fan_volume1_50);
        FanListOfimages.add(R.drawable.fan_volume2_50);
        FanListOfimages.add(R.drawable.fan_volume3_50);
        FanListOfimages.add(R.drawable.fan_volume4_50);
        ModeSpinnerAdapter spinnerAdapterFan=new ModeSpinnerAdapter(getApplicationContext(),FanListOfimages);
        fanVolume.setAdapter(spinnerAdapterFan);
        fanVolume.setOnItemSelectedListener(this);
        for(int i =0 ; i<FanListOfimages.size();i++){
            if(imageFan==FanListOfimages.get(i)){
                fanVolume.setSelection(i);
                fanVolumeSelect = FanListOfimages.get(i);
            }
        }
        swivelList = new ArrayList<>();
        swivelList.add("Αυτόματο");
        swivelList.add("Κίνηση πάνω-κάτω");
        swivelList.add("Κάτω");
        swivelList.add("Πλάγια");
        swivelList.add("Ευθεία");
        ArrayAdapter<String> adapterSwivel =new ArrayAdapter(getApplicationContext(),android.R.layout.simple_spinner_item,swivelList);
        swivelSel.setAdapter(adapterSwivel);


        advancedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (advanced){
                    advanced = false;
                    turnOff.setVisibility(View.GONE);
                    turnOn.setVisibility(View.GONE);
                    repeat.setVisibility(View.GONE);
                    timerOff.setVisibility(View.GONE);
                    timerOn.setVisibility(View.GONE);
                    repeatButton.setVisibility(View.GONE);
                }
                else{
                    advanced = true;
                    turnOff.setVisibility(View.VISIBLE);
                    turnOn.setVisibility(View.VISIBLE);
                    repeat.setVisibility(View.VISIBLE);
                    timerOff.setVisibility(View.VISIBLE);
                    timerOn.setVisibility(View.VISIBLE);
                    repeatButton.setVisibility(View.VISIBLE);
                }
            }
        });


        celcius.setText(Integer.toString(temperature) + "°C");

        deviceName.setText(device);
        final boolean[] sw_temperature = {false};

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(temperature>=27 && temperature<=29 && modeSelect == R.drawable.sun_50  ) {
                    if (!sw_high_temp) {
                        new AlertDialog.Builder(ControlActivity.this)
                                .setTitle("Επιβεβαίωση θερμοκρασίας")
                                .setMessage("Προσοχή υψηλή τιμή θερμοκρασίας! Επιβεβαιώστε ότι θέλετε να συνεχίσετε.")
                                .setPositiveButton("ΣΥΝΕΧΕΙΑ", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        sw_temperature[0] = true;
                                        temperature++;
                                        sw_high_temp = true;
                                        celcius.setText(Integer.toString(temperature) + "°C");
                                    }
                                })

                                // A null listener allows the button to dismiss the dialog and take no further action.
                                .setNegativeButton("ΑΚΥΡΩΣΗ", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        sw_high_temp = false;
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();

                    }
                    else{
                        sw_temperature[0] = true;
                        temperature++;
                        sw_high_temp = true;
                        celcius.setText(Integer.toString(temperature) + "°C");
                    }
                }
                else  if(temperature>29 ){
                    Toast.makeText(ControlActivity.this, "Δεν υποστηρίζεται αυτό το επίπεδο θερμοκρασίας από την συσκευή", Toast.LENGTH_LONG).show();
                    celcius.setText(Integer.toString(temperature) + "°C");
                }

                else  {
                    temperature++;
                    celcius.setText(Integer.toString(temperature) + "°C");
                }
                celcius.setText(Integer.toString(temperature) + "°C");

            }
        });

        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( temperature>16 && temperature<=18 && modeSelect == R.drawable.snowflake_50 ) {
                    if (!sw_low_temp) {
                        new AlertDialog.Builder(ControlActivity.this)
                                .setTitle("Επιβεβαίωση θερμοκρασίας")
                                .setMessage("Προσοχή υπερβολικά χαμηλή τιμή θερμοκρασίας! Επιβεβαιώστε ότι θέλετε να συνεχίσετε.")


                                .setPositiveButton("ΣΥΝΕΧΕΙΑ", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        sw_temperature[0] = true;
                                        temperature--;
                                        sw_low_temp = true;
                                        celcius.setText(Integer.toString(temperature) + "°C");
                                    }
                                })

                                // A null listener allows the button to dismiss the dialog and take no further action.
                                .setNegativeButton("ΑΚΥΡΩΣΗ", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        sw_low_temp = false;
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();

                    }
                    else{
                        sw_temperature[0] = true;
                        temperature--;
                        sw_high_temp = true;
                        celcius.setText(Integer.toString(temperature) + "°C");
                    }
                }
                else  if(temperature<=16 ){
                    Toast.makeText(ControlActivity.this, "Δεν υποστηρίζεται αυτό το επίπεδο θερμοκρασίας από την συσκευή", Toast.LENGTH_LONG).show();
                    celcius.setText(Integer.toString(temperature) + "°C");
                }

                else  {
                    temperature--;
                    celcius.setText(Integer.toString(temperature) + "°C");
                }
                celcius.setText(Integer.toString(temperature) + "°C");
            }
        });


        on_off.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sw_on_off){
                    sw_on_off = false;
                    on_off.setText("Κλεισε συσκευη");
                    swivel.setVisibility(View.VISIBLE);
                    swivelSel.setVisibility(View.VISIBLE);
                    mode.setVisibility(View.VISIBLE);
                    volumeOfFan.setVisibility(View.VISIBLE);
                    eco.setVisibility(View.VISIBLE);
                    save_preset.setVisibility(View.VISIBLE);
                    celcius.setVisibility(View.VISIBLE);
                    turnOff.setVisibility(View.VISIBLE);
                    turnOn.setVisibility(View.VISIBLE);
                    repeat.setVisibility(View.VISIBLE);
                    plus.setVisibility(View.VISIBLE);
                    minus.setVisibility(View.VISIBLE);
                    modeList.setVisibility(View.VISIBLE);
                    advancedBtn.setVisibility(View.VISIBLE);
                    timerOff.setVisibility(View.VISIBLE);
                    timerOn.setVisibility(View.VISIBLE);
                    repeatButton.setVisibility(View.VISIBLE);
                    fanVolume.setVisibility(View.VISIBLE);
                    Toast.makeText(ControlActivity.this, "Η συσκευή: " +device+" ξεκίνησε την λειτουργία της", Toast.LENGTH_SHORT).show();
                }
                else {
                    sw_on_off = true;
                    on_off.setText("Ανοιξε συσκευη");
                    swivelSel.setVisibility(View.GONE);
                    swivel.setVisibility(View.GONE);
                    mode.setVisibility(View.GONE);
                    volumeOfFan.setVisibility(View.GONE);
                    eco.setVisibility(View.GONE);
                    save_preset.setVisibility(View.GONE);
                    celcius.setVisibility(View.GONE);
                    turnOff.setVisibility(View.GONE);
                    turnOn.setVisibility(View.GONE);;
                    repeat.setVisibility(View.GONE);
                    plus.setVisibility(View.GONE);;
                    minus.setVisibility(View.GONE);;
                    modeList.setVisibility(View.GONE);;
                    advancedBtn.setVisibility(View.GONE);;
                    timerOff.setVisibility(View.GONE);
                    timerOn.setVisibility(View.GONE);
                    repeatButton.setVisibility(View.GONE);
                    fanVolume.setVisibility(View.GONE);;
                    Toast.makeText(ControlActivity.this, "Η συσκευή: " +device+" σταμάτησε την λειτουργία της", Toast.LENGTH_SHORT).show();
                }
            }
        });

        timerOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = "Επιλέξτε την ώρα που θέλετε να κλείσει η συσκευή αυτόματα";
                spinersInAdialog(message, "κλείσει",turnOff);


            }
        });

        timerOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = "Επιλέξτε την ώρα που θέλετε να ανοίξει η συσκευή αυτόματα";
                spinersInAdialog(message, "ανοίξει",turnOn);

            }
        });

        repeatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repeat_Boxes(repeat);
            }
        });



    }

    private void repeat_Boxes(final TextView tv){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(ControlActivity.this);
        mBuilder.setTitle("Επανάληψη προγράμματος ανάλογα τις μέρες της εβδομάδας");
        final String[] days = new String[7];
        days[0] = "Δευτέρα";
        days[1] = "Τρίτη";
        days[2] = "Τετάρτη";
        days[3] = "Πέμπτη";
        days[4] = "Παρασκευή";
        days[5] = "Σάββατο";
        days[6] = "Κυριακή";

        final boolean[] checkedItems = new boolean[days.length];

        mBuilder.setMultiChoiceItems(days, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position, boolean isChecked) {

            }
        });

        mBuilder.setCancelable(false);
        mBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String item = "";
                int counter = 0;
                int added = 0;
                for (int i = 0; i < checkedItems.length; i++) {
                    if(checkedItems[i]){
                        counter++;
                    }
                }
                for (int i = 0; i < checkedItems.length; i++) {
                    if(checkedItems[i]){
                        added++;
                        item = item + days[i] ;
                        if(added <counter){
                            item = item + ", ";
                        }
                    }

                }
                tv.setText(item);
            }
        });

        mBuilder.setNegativeButton("Ακυρο", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                tv.setText("");

            }
        });

        AlertDialog mDialog = mBuilder.create();
        mDialog.show();
    }

    private void spinersInAdialog(String message, final String open_close, final TextView tv){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(ControlActivity.this);
        mBuilder.setTitle("Χρονομετρητής κλιματιστικού");
        mBuilder.setMessage(message);
        final EditText emailAddress = new EditText(ControlActivity.this);


        final Spinner Hour = new Spinner(ControlActivity.this);
        final Spinner Min= new Spinner(ControlActivity.this);
        final Spinner Day_Night= new Spinner(ControlActivity.this);



        ArrayList<String> hours = new ArrayList<>();
        ArrayList<String> min = new ArrayList<>();
        ArrayList<String> dayNight = new ArrayList<>();

        LinearLayout layout = new LinearLayout(ControlActivity.this);
        layout.setOrientation(LinearLayout.VERTICAL);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        layout.addView(Hour);
        layout.addView(Min);
        layout.addView(Day_Night);
        mBuilder.setView(layout);
        // Hour.setLayoutParams(lp);
        hours.add("Ώρα");
        for (int i =1;i<=12;i++){

            if (i<10) hours.add("0"+Integer.toString(i));
            else hours.add(Integer.toString(i));
        }
        min.add("Λεπτά");
        for (int i =0;i<60;i++){

            if (i<10) min.add("0"+Integer.toString(i));
            else min.add(Integer.toString(i));
        }
        dayNight.add("Π.Μ."); dayNight.add("Μ.Μ.");
        ArrayAdapter<String> adapterHour =new ArrayAdapter(getApplicationContext(),android.R.layout.simple_spinner_item,hours);
        adapterHour.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter<String> adapterMin =new ArrayAdapter(getApplicationContext(),android.R.layout.simple_spinner_item,min);
        adapterHour.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter<String> adapterDayNight =new ArrayAdapter(getApplicationContext(),android.R.layout.simple_spinner_item,dayNight);
        adapterDayNight.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Hour.setAdapter(adapterHour);
        Min.setAdapter(adapterMin);
        Day_Night.setAdapter(adapterDayNight);



        mBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(!Hour.getSelectedItem().toString().equals("Ώρα") && !Min.getSelectedItem().toString().equals("Λεπτά") ) {

                    String text = Hour.getSelectedItem().toString() + ":"
                            + Min.getSelectedItem().toString() + " "
                            + Day_Night.getSelectedItem().toString();
                    tv.setText(text);
                    int hour = Integer.parseInt(Hour.getSelectedItem().toString());
                    int min = Integer.parseInt(Min.getSelectedItem().toString());
                    String dayNight = Day_Night.getSelectedItem().toString();
                    if (dayNight.equals("M.M.")) hour +=12;
                    SimpleDateFormat sdf = new SimpleDateFormat("HH", Locale.getDefault());
                    String currentTime = sdf.format(new Date());
                    int current_hour = Integer.parseInt(currentTime);
                    sdf = new SimpleDateFormat("mm", Locale.getDefault());
                    currentTime = sdf.format(new Date());
                    int current_min = Integer.parseInt(currentTime);
                    if (current_min<min) min -= current_min;
                    else {
                        min = min + 60 - current_min;
                        hour -= 1;
                        if (hour<0) hour += 24;
                    }
                    if (current_hour<=hour) hour -= current_hour;
                    else hour = hour + 24 - current_hour;
                    Toast.makeText(ControlActivity.this, "Η συσκευή του κλιματιστικού θα "+open_close+" σε "+ hour + " ώρες και " + min + " λεπτά.", Toast.LENGTH_LONG).show();
                }
            }
        });

        mBuilder.setNegativeButton("Ακυρο",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        tv.setText("");
                        return;
                    }
                });
        final AlertDialog mDialog = mBuilder.create();
        mDialog.show();

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        //for selecting a mode image
        if(parent.getId() == R.id.control_temp_mode_spnr)
        {
            Integer item = modeListOfimages.get(position);

            if(item!=null) {
                if(item == R.drawable.sun_50 && temperature>=27){
                    temperature = 26;
                    celcius.setText(Integer.toString(temperature) + "°C");
                    Toast.makeText(ControlActivity.this, "Χαμηλώσαμε τα επίπεδα της θερμοκρασίας λόγω της αλλαγής από κρύο σε ζέστη", Toast.LENGTH_LONG).show();
                }
                if(item == R.drawable.snowflake_50 && temperature<=20 ){
                    temperature = 23;
                    celcius.setText(Integer.toString(temperature) + "°C");
                    Toast.makeText(ControlActivity.this, "Προσαρμόσαμε τα επίπεδα της θερμοκρασίας λόγω της αλλαγής από ζέστη/υγρασία σε κρύο", Toast.LENGTH_LONG).show();
                }
                modeSelect = item;
                System.out.println("mode int: " + modeSelect);
            }

        }

        //for selecting a fan volume image
        else if(parent.getId() == R.id.control_fan_speed_spnr )
        {
            Integer item = FanListOfimages.get(position);
            if(item!=null ) {
                fanVolumeSelect = item;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void savePreset(View v, HashMap<String, PresetListItem> selected_preset){

        final PresetListItem new_preset = new PresetListItem(modeSelect, fanVolumeSelect, "Νέο Πρόγραμμα", String.valueOf(temperature));

        for(Map.Entry<String, PresetListItem> entry : selected_preset.entrySet()){
            if(new_preset.getTemperature().equals(entry.getValue().getTemperature()) && new_preset.getFanVolume()==entry.getValue().getFanVolume() && new_preset.getImages() == entry.getValue().getImages()){
                Toast.makeText(ControlActivity.this, "Ένα πρόγραμμα με τα ίδια χαρακτηριστικά υπάρχει ήδη. Η αποθήκευση δεν θα πραγματοποιηθεί για εξοικονόμηση χώρου.", Toast.LENGTH_LONG ).show();
            }
            else{

                final boolean[] press_ok = {false};

                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                final EditText preset_name = new EditText(ControlActivity.this);
                alert.setMessage("Δώστε ένα όνομα για το πρόγραμμα");
                alert.setTitle("Νέο πρόγραμμα");

                alert.setView(preset_name);

                alert.setPositiveButton("ΟΚ", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        new_preset.setmImageNames(preset_name.getText().toString());

                        FileOutputStream fos = null;
                        try {
                            fos = openFileOutput("presets.txt",MODE_APPEND);
                            fos.write(String.valueOf(new_preset.getImages()).getBytes());
                            fos.write("^&*)^#$1".getBytes());
                            fos.write(String.valueOf(new_preset.getFanVolume()).getBytes());
                            fos.write("^&*)^#$2".getBytes());
                            fos.write(new_preset.getmImageNames().getBytes());
                            fos.write("^&*)^#$3".getBytes());
                            fos.write((new_preset.getTemperature() + "°C").getBytes());
                            fos.write("\n".getBytes());
                            fos.close();


                            Toast.makeText(ControlActivity.this,"Αποθήκευση " + new_preset.getmImageNames(),Toast.LENGTH_SHORT).show();
                        }catch (FileNotFoundException e){
                            e.printStackTrace();
                        }catch (IOException e){
                            e.printStackTrace();
                        }finally {
                            if(fos != null) {
                                try {
                                    fos.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        Intent intent = new Intent(ControlActivity.this, PresetActivity.class);
                        intent.putExtra("message_device", device);
                        startActivity(intent);
                    }
                });

                alert.setNegativeButton("ΑΚΥΡΟ", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                    }
                });

                alert.show();
            }
        }
    }


}
