package com.example.air_conditioning.ui.devices;

import android.os.Parcel;
import android.os.Parcelable;

public class DeviceListItem implements Parcelable {
    private String name;
    private String code;
    private int status;

    public DeviceListItem(String name , String code , int status){
        this.name = name;
        this.code = code;
        this.status = status;
    }


    protected DeviceListItem(Parcel in) {
        name = in.readString();
        code = in.readString();
        status = in.readInt();
    }

    public static final Creator<DeviceListItem> CREATOR = new Creator<DeviceListItem>() {
        @Override
        public DeviceListItem createFromParcel(Parcel in) {
            return new DeviceListItem(in);
        }

        @Override
        public DeviceListItem[] newArray(int size) {
            return new DeviceListItem[size];
        }
    };

    public String getCode() {
        return code;
    }

    public int getStatus() {
        return status;
    }

    public String getName() {
        return name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(code);
        dest.writeInt(status);
    }
}
