package com.example.air_conditioning.ui.devices;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.air_conditioning.R;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

public class DeviceListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private ArrayList<DeviceListItem> mCardList;
    private OnItemClickListener mListener;

    @Override
    public Filter getFilter() {
        return null;
    }


    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }


    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView mTextViewName;
        private TextView mTextViewCode;
        private ImageView mImageView;

        public ItemViewHolder(@NonNull View itemView , final OnItemClickListener listener) {
            super(itemView);
            mTextViewName = itemView.findViewById(R.id.device_item_name);
            mTextViewCode = itemView.findViewById(R.id.device_item_code);
            mImageView = itemView.findViewById(R.id.device_item_status);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }

        public void setItemData(DeviceListItem item){
            mTextViewName.setText(item.getName());
            mTextViewCode.setText(item.getCode());
            mImageView.setImageResource(item.getStatus());
        }
    }


    public DeviceListAdapter(ArrayList<DeviceListItem> itemList) {
        mCardList = itemList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ItemViewHolder(
                    LayoutInflater.from(parent.getContext()).inflate(
                            R.layout.device_list_item, parent, false
                    ),mListener
            );


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            DeviceListItem item = (DeviceListItem) mCardList.get(position);
            ((ItemViewHolder) holder).setItemData(item);

    }

    @Override
    public int getItemCount() {
        return mCardList.size();
    }


}
