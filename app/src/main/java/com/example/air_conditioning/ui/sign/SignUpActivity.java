package com.example.air_conditioning.ui.sign;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.air_conditioning.R;
import com.example.air_conditioning.ui.login.LoginActivity;

public class SignUpActivity extends AppCompatActivity {
    TextView textSign;
    EditText email;
    EditText userName;
    EditText password;
    EditText repeatPassword;
    Button sign;
    Button cancel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        textSign = (TextView)findViewById(R.id.tvSign);
        email = (EditText)findViewById(R.id.ed_email);
        userName = (EditText)findViewById(R.id.ed_userName);
        password = (EditText)findViewById(R.id.ed_password);
        repeatPassword = (EditText)findViewById(R.id.ed_repeatPass);
        sign = (Button)findViewById(R.id.btn_sign);
        cancel = (Button)findViewById(R.id.btn_cancel);

        sign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                if (!userName.getText().toString().trim().equals("") && !email.getText().toString().trim().equals("") && !password.getText().toString().trim().equals("") && !repeatPassword.getText().toString().trim().equals("") && isPasswordValid(password.getText().toString().trim()) && isEmailValid(email.getText().toString().trim())) {
                    if (password.getText().toString().trim().equals(repeatPassword.getText().toString().trim())) {
                        Toast.makeText(SignUpActivity.this, "Εγγραφήκατε με επιτυχία!", Toast.LENGTH_SHORT);
                        LoginActivity.userdata.put(userName.getText().toString().trim(), password.getText().toString().trim());
                        startActivity(intent);
                    } else {
                        repeatPassword.setError("Έγινε κάποιο λάθος. Συμπληρώστε ξανά τον κωδικό");
                        Toast.makeText(SignUpActivity.this, "Παρακαλώ ελέγξτε τον κωδικό που συμπλρώσατε!", Toast.LENGTH_SHORT).show();
                    }
                }
                if(email==null || email.getText().toString().trim().equals("") ){
                    email.setError("Παρακαλώ συμπληρώστε διεύθυνση email");
                }
                if(userName==null || userName.getText().toString().trim().equals("") ){
                    userName.setError("Παρακαλώ συμπληρώστε όνομα χρήστη");
                }
                if(password==null || password.getText().toString().trim().equals("") ){
                    password.setError("Παρακαλώ συμπληρώστε κωδικό χρήστη");
                }
                if(repeatPassword==null || repeatPassword.getText().toString().trim().equals("") ){
                    repeatPassword.setError("Παρακαλώ συμπληρώστε τον κωδικό σας ξανά");
                }
                if(!isPasswordValid(password.getText().toString().trim())){
                    password.setError(getString(R.string.invalid_password));
                }
                if(!isEmailValid(email.getText().toString().trim())){
                    email.setError(getString(R.string.invalid_email));
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(SignUpActivity.this, "Ακύρωση εγγραφής", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });


    }

    private boolean isEmailValid(String email) {
        if (email == null) {
            return false;
        }
        if (!email.contains("@")) {
            return false;
        } else {
            return true;
        }
    }

    // A placeholder password validation check
    private boolean isPasswordValid(String password) {
        return password.trim().length() >= 5;
    }
}