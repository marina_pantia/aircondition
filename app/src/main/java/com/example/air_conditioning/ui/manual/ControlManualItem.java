package com.example.air_conditioning.ui.manual;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.air_conditioning.ui.presets.PresetListItem;

public class ControlManualItem implements Parcelable {

    private int image ;
    private String info;

    public ControlManualItem(int image, String info) {
        this.image = image;
        this.info = info;
    }



    protected ControlManualItem(Parcel in) {
        this.image =  in.readInt();
        this.info = in.readString();
    }

    public static final Creator<ControlManualItem> CREATOR = new Creator<ControlManualItem>() {
        @Override
        public ControlManualItem createFromParcel(Parcel in) {
            return new ControlManualItem(in);
        }

        @Override
        public ControlManualItem[] newArray(int size) {
            return new ControlManualItem[size];
        }
    };


    public int getImage() {
        return image;
    }

    public String getInfo() {
        return info;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(image);
        dest.writeString(info);
    }

}
