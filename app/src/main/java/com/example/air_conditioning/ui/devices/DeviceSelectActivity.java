package com.example.air_conditioning.ui.devices;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.example.air_conditioning.R;
import com.example.air_conditioning.ui.login.LoginActivity;
import com.example.air_conditioning.ui.main.MainActivity;
import com.example.air_conditioning.ui.manual.AdvancedManual;
import com.example.air_conditioning.ui.manual.ControlManual;
import com.example.air_conditioning.ui.manual.PresetManual;
import com.example.air_conditioning.ui.presets.PresetActivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class DeviceSelectActivity extends AppCompatActivity {
    private RecyclerView devicesList;
    private Button syncDevice;

    private DeviceListAdapter mAdapter;
    private RecyclerView.LayoutManager mLayouteManager;
    private ArrayList<DeviceListItem> mCardList;


    private void Logout() {
        LoginActivity login = new LoginActivity();
        if (!login.getSkipped()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(DeviceSelectActivity.this);
            builder.setTitle("Μήνυμα επιβεβαίωσης");
            builder.setMessage("Είστε σίγουροι ότι θέλετε να αποσυνδεθείτε από τον λογαριασμό σας;");

            builder.setCancelable(true);
            builder.setPositiveButton("ΝΑΙ", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();
                    Intent intent = new Intent(DeviceSelectActivity.this, LoginActivity.class);
                    startActivity(intent);
                    return;

                }
            });
            builder.setNegativeButton("ΟΧΙ",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            return;
                        }
                    });
            builder.show();
        } else {
            Toast.makeText(DeviceSelectActivity.this, "Δεν έχετε συνδεθεί σε κάποιο λογαριασμό για να αποσυνδεθείτε", Toast.LENGTH_LONG).show();
        }
    }

    private void Manual() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(DeviceSelectActivity.this);
        builder.setTitle("Εγχειρίδιο");
        builder.setMessage("Ενότητες: ");

        final ListView unitList = new ListView(DeviceSelectActivity.this);

        ArrayList<String> listunit = new ArrayList<>();
        final String unitPresets = "Ενότητα 1η: Έτοιμα Προγραμμάτα";
        final String unitControl = "Ενότητα 2η: Τηλεχειριστήριο και εικονίδια";
        final String unitAdvanced = "Ενότητα 3η: Λειτουργία για προχωρημένους";

        listunit.add(unitPresets);
        listunit.add(unitControl);
        listunit.add(unitAdvanced);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(DeviceSelectActivity.this, android.R.layout.simple_list_item_1, android.R.id.text1, listunit);

        unitList.setAdapter(adapter);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        unitList.setLayoutParams(lp);
        builder.setView(unitList);
        builder.setCancelable(true);
        builder.setPositiveButton("ΟΚ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                return;

            }
        });

        builder.show();

        unitList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = (String) unitList.getItemAtPosition(position);
                if(item.equals(unitPresets)) {
                    Intent intent1 = new Intent(DeviceSelectActivity.this, PresetManual.class);
                    startActivity(intent1);
                }
                else if(item.equals(unitControl)) {
                    Intent intent2 = new Intent(DeviceSelectActivity.this, ControlManual.class);
                    startActivity(intent2);
                }
                else {
                    Intent intent3 = new Intent(DeviceSelectActivity.this, AdvancedManual.class);
                    startActivity(intent3);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.manual: {
                Manual();
                System.err.println("Manual");
                break;
            }
            case R.id.logout: {
                Logout();
                break;
            }
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_select);

        syncDevice = findViewById(R.id.devices_sync);


        mCardList = loadDevices();

        buildRecyclerView();


        syncDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), DevicesSyncActivity.class);
                startActivity(intent);
            }
        });

    }

    public void buildRecyclerView(){
        devicesList = findViewById(R.id.device_select_recycler_view);
        devicesList.setHasFixedSize(true);
        mLayouteManager = new LinearLayoutManager(this);
        mAdapter = new DeviceListAdapter(mCardList);
        devicesList.setLayoutManager(mLayouteManager);
        devicesList.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new DeviceListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                //Do stuff
                Intent intent = new Intent(DeviceSelectActivity.this , PresetActivity.class);
                intent.putExtra("message_device",mCardList.get(position).getName());
                startActivity(intent);
            }
        });
    }

    public ArrayList<DeviceListItem> loadDevices(){
        FileInputStream fis = null;
        ArrayList<DeviceListItem> devices = new ArrayList<>();
        try {
            fis = openFileInput("devices.txt");
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            String text;
            while((text = br.readLine())!=null){
                String code;
                if((code = br.readLine())!=null){
                    devices.add(new DeviceListItem(text,code,R.drawable.ic_baseline_device_status_off_24));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return devices;
    }
}