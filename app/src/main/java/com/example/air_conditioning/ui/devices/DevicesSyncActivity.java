package com.example.air_conditioning.ui.devices;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.air_conditioning.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class DevicesSyncActivity extends AppCompatActivity {

    private EditText name;
    private EditText code;
    private Button syncBtn;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_sync);
        name = findViewById(R.id.device_name_edtext);
        code = findViewById(R.id.device_code_edtext);
        syncBtn = findViewById(R.id.device_sync_btn);

        syncBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(name.getText().length()>0 && code.getText().length()>0){
                    saveDevice(v);
                    Intent intent = new Intent(v.getContext() , DeviceSelectActivity.class);
                    startActivity(intent);
                }
                else{
                    Toast.makeText(v.getContext(),"Παρακαλώ συμπληρώστε τα παραπάνω πεδία",Toast.LENGTH_SHORT).show();
                }
            }
        });





}

    public void saveDevice(View v){
        String editTextName = name.getText().toString();
        String editTextCode = code.getText().toString();

        FileOutputStream fos = null;

        try {
            fos = openFileOutput("devices.txt",MODE_APPEND);
            fos.write(editTextName.getBytes());
            fos.write("\n".getBytes());
            fos.write(editTextCode.getBytes());
            fos.write("\n".getBytes());
            Toast.makeText(this,"Αποθηκευμένη συσκευή " + editTextName,Toast.LENGTH_SHORT).show();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            if(fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }



    }
}
