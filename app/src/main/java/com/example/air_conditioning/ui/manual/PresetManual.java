package com.example.air_conditioning.ui.manual;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.air_conditioning.R;

public class PresetManual extends AppCompatActivity {
    TextView title_our_presets;
    TextView title_your_presets;
    TextView our_presets_info;
    TextView your_presets_info;
    Button back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preset_manual);

        System.out.println("inside preset manual");
        title_our_presets = (TextView)findViewById(R.id.tv_our_presets_manual);
        title_your_presets = (TextView)findViewById(R.id.tv_your_presets_manual);
        our_presets_info = (TextView)findViewById(R.id.tv_info_manual_our_presets);
        your_presets_info = (TextView)findViewById(R.id.tv_info_manual_your_presets);
        back =(Button)findViewById(R.id.btn_back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("go back");
                finish();
            }
        });

    }
}