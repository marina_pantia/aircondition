package com.example.air_conditioning.ui.manual;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.example.air_conditioning.R;
import com.example.air_conditioning.ui.main.ControlActivity;
import com.example.air_conditioning.ui.presets.PresetActivity;
import com.example.air_conditioning.ui.presets.PresetListItem;
import com.example.air_conditioning.ui.presets.RecyclerViewAdapter;

import java.util.ArrayList;
import java.util.HashMap;

public class ControlManual extends AppCompatActivity {

    RecyclerView infoList;
    ArrayList<Integer> images = new ArrayList<>();
    ArrayList<String> information = new ArrayList<>();
    ArrayList<ControlManualItem> manualItem = new ArrayList<>();
    Button back;
    ListView informationList;
    ArrayList<String> list_info = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control_manual);
        infoList = (RecyclerView) findViewById(R.id.rv_info_manual);
        back = (Button)findViewById(R.id.btn_back_from_control);
        informationList = (ListView)findViewById(R.id.lv_info_manual) ;

        list_info.add("Περσίδες : Η λειτουργία περσίδες αφορά την κίνηση των περσίδων του κλιματιστικού σας" +
                " κάθετα και οριζόντια (με τις τιμές (κάτω, πλάγια και ευθεία). Η επιλογή κίνηση " +
                "πάνω-κάτω κάνει τις περσίδες να κινούνται πάνω-κάτω." +
                " Η επιλογή αυτόματο ρυθμίζει τις περσίδες να πέρνουν μια συγκεκριμένη θέση κάθετα και οριζόντια.");

        list_info.add("ECO : η επιλογή ECO ρυθμίζει τη συσκευή σας σε συγκεκριμένη θερμοκρασία " +
                "και δύναμη ανεμιστήρα με σκοπό την μικρότερη δυνατή κατανάλωση ρεύματος. " +
                "Όταν είναι ενεργή δεν μπορείτε να ρυθμίσετε τα επίπεδα θερμοκρασίας και" +
                " την δύναμη ανεμιστήρα. Μπορείτε να την απενεργοποιήσετε κάνοντας κλικ πάνω στο κουμπί ECΟ.");

        list_info.add("Κλείσε συσκευή - Άνοιξε συσκευή : Κάνοντας κλικ σε αυτό το κουμπί κλείνετε και ανοίγετε την λειτουργία του κλιματιστικού " +
                "σας αντίστοιχα. Όταν κάνετε κλικ στο Κλείσε συσκευή όλα τα στοιχεία του τηλεχειριστηρίου σταματάνε να είναι εμφανή καθώς η συσκευή σας " +
                "είναι πλέον κλειστή. Όταν πατήσετε το κουμπί Άνοιξε συσκευή τα στοιχεία του κοντρόλ θα εμφανιστούν ξανά " +
                "και το κλιματιστικό σας είναι έτοιμο για χρήση.");

        list_info.add("Αποθήκευση: κάνοντας κλικ στην επιλογή αποθήκευση καταχωρείτε το πρόγραμμα που μόλις φτιάξατε " +
                "στη λίστα με τα προγράμματά σας (Τα προγράμματά μου) βάζοντας ένα όνομα της προτίμησης σας. Για περισσότερες πληροφορίες " +
                "για τα έτοιμα προγράμματα μεταβείτε στο μενού πληροφορίες -> Ενότητα 1η: Έτοιμα Προγραμμάτα.");

        ArrayAdapter<String> adapter =  new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list_info);

        informationList.setAdapter(adapter);
        initImages(manualItem, images, information);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initImages(ArrayList<ControlManualItem> manualItem, ArrayList<Integer> images, ArrayList<String> information) {

        images.add(R.drawable.snowflake_32);
        information.add("Λειτουργία ψύξης");
        images.add(R.drawable.sun_32);
        information.add("Λειτουργία θέρμανσης");
        images.add(R.drawable.rain_32);
        information.add("Λειτουργία υγραντήρα");
        images.add(R.drawable.minus_plus_32);
        information.add("Αύξηση κατά ένα βαθμό της θερμοκρασίας");
        images.add(R.drawable.minus_alone_32);
        information.add("Μείωση κατά ένα βαθμό της θερμοκρασίας");
        images.add(R.drawable.fan_volume4_32);
        information.add("Δύναμη ανεμιστήρα : Κάθε κάθετη γραμμή αντιστοιχεί σε ένα επιπέδο ανεμιστήρα (σε αυτή την εικόνα το επιπέδο είναι 4");


        for (int i = 0; i < images.size(); i++) {
            ControlManualItem item = new ControlManualItem(images.get(i), information.get(i));
            manualItem.add(item);
        }
        initRecyclerView(manualItem);
    }

    private void initRecyclerView(ArrayList<ControlManualItem> listItems) {

        final RecyclerAdapter adapter = new RecyclerAdapter(listItems);
        infoList.setAdapter(adapter);
        infoList.setLayoutManager(new LinearLayoutManager(ControlManual.this));

    }

}