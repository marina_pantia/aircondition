package com.example.air_conditioning.ui.manual;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.air_conditioning.R;

public class AdvancedManual extends AppCompatActivity {

    Button back;
    TextView open_close;
    TextView repeat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advanced_manual);
        back = (Button)findViewById(R.id.btn_back_advanced);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        open_close = (TextView) findViewById(R.id.tv_open_close_manual);
        open_close.setText("Πατώντας το κουμπί ΚΛΕΙΣΕ ή ΑΝΟΙΞΕ θα ανοίξει μία οθόνη στην οποία θα σας ζητηθεί "+
                "να επιλέξετε την ώρα που θέλετε να κλείσει ή να ανοίξει αντίστοιχα η συσκευή του κλιματιστικού. "+
                "Για να ολοκληρώσετε την επιλογή σας πατάτε το κουμπί ΟΚ. Αν δεν επιλέξετε κάποια ώρα ή πατήσετε το κουμπί ΑΚΥΡΟ τότε η επιλογή ακυρώνετε.");
        repeat = (TextView) findViewById(R.id.repeat_tv);
        repeat.setText("Πατώντας το κουμπί ΕΠΑΝΕΛΑΒΕ θα ανοίξει μία οθόνη η οποία θα σας ζητήσει να επιλέξετε "+
                "τις μέρες τις οποίες θα θέλατε ο προγραμματισμός για το άνοιγμα και το κλείσιμο της συσκευής που έχετε "+
                "επιλέξει να επαναλαμβάνεται.Για να ολοκληρώσετε την επιλογή σας πατάτε το κουμπί ΟΚ. Αν δεν "+
                "επιλέξετε κάποια ώρα ή πατήσετε το κουμπί ΑΚΥΡΟ τότε η επιλογή ακυρώνετε.");
    }
}