package com.example.air_conditioning.ui.manual;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.air_conditioning.R;
import com.example.air_conditioning.ui.presets.PresetListItem;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.air_conditioning.R;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends  RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable  {
    private ArrayList<ControlManualItem> list;
    private RecyclerAdapter.OnItemClickListener mListener;


    @Override
    public Filter getFilter() {
        return null;
    }


    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    public void setOnItemClickListener(RecyclerAdapter.OnItemClickListener listener){
        mListener = listener;
    }


    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        private ImageView mImageView;
        private TextView mTextViewName;

        public ItemViewHolder(@NonNull final View itemView , final RecyclerAdapter.OnItemClickListener listener) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.iv_image_info);
            mTextViewName = itemView.findViewById(R.id.tv_image_info);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }

        public void setItemData(ControlManualItem item){
            mImageView.setImageResource(item.getImage());
            mTextViewName.setText(item.getInfo());

        }
    }


    public RecyclerAdapter(ArrayList<ControlManualItem> itemList) {
        list = itemList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerAdapter.ItemViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.layout_recycler_list_item, parent, false
                ),mListener
        );


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ControlManualItem item = (ControlManualItem) list.get(position);
        ((RecyclerAdapter.ItemViewHolder) holder).setItemData(item);


    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}







