package com.example.air_conditioning.ui.login;

import android.annotation.SuppressLint;
import android.app.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.air_conditioning.R;
import com.example.air_conditioning.ui.main.MainActivity;
import com.example.air_conditioning.ui.sign.SignUpActivity;

import java.util.HashMap;
import java.util.Map;

import static java.lang.Integer.parseInt;

public class LoginActivity extends AppCompatActivity {

    public static String name = "";
    public static String password = "";
    private LoginViewModel loginViewModel;
    public static Map<String, String> userdata = new HashMap<>();
    TextView forgot;
    public static boolean skipped = false;

    public boolean getSkipped(){
        return this.skipped;
    }
    public void setSkipped(boolean skipped){
        this.skipped = skipped;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginViewModel = ViewModelProviders.of(this, new LoginViewModelFactory())
                .get(LoginViewModel.class);

        final EditText usernameEditText = findViewById(R.id.username);
        final EditText passwordEditText = findViewById(R.id.password);
        final Button loginButton = findViewById(R.id.btn_login);
        final Button register = findViewById(R.id.btn_register);
        final Button skip = findViewById(R.id.btn_skip);
        final ProgressBar loadingProgressBar = findViewById(R.id.loading);
        forgot = (TextView)findViewById(R.id.tvforgot);
        name = usernameEditText.getText().toString().trim();

        userdata.put("marina", "1234567");
        userdata.put("elias", "12345678");
        userdata.put("kostas", "123456789");

        loginViewModel.getLoginFormState().observe(this, new Observer<LoginFormState>() {
            @Override
            public void onChanged(@Nullable LoginFormState loginFormState) {
                if (loginFormState == null) {
                    return;
                }
                loginButton.setEnabled(loginFormState.isDataValid());
                if (loginFormState.getUsernameError() != null) {
                    usernameEditText.setError(getString(loginFormState.getUsernameError()));
                }
                if (loginFormState.getPasswordError() != null) {
                    passwordEditText.setError(getString(loginFormState.getPasswordError()));
                }
            }
        });

        loginViewModel.getLoginResult().observe(this, new Observer<LoginResult>() {
            @Override
            public void onChanged(@Nullable LoginResult loginResult) {
                if (loginResult == null) {
                    return;
                }
                loadingProgressBar.setVisibility(View.GONE);
               /* if (loginResult.getError() != null) {
                    showLoginFailed(loginResult.getError());
                }*/
                if (loginResult.getSuccess() != null) {
                    updateUiWithUser(loginResult.getSuccess());
                }
                setResult(Activity.RESULT_OK);

                //Complete and destroy login activity once successful
                name = usernameEditText.getText().toString().trim();
                password = passwordEditText.getText().toString().trim();
                boolean logIn = false;
                for(Map.Entry<String, String> entry : userdata.entrySet()){
                    String instanceName = entry.getKey();
                    String instancePassword = entry.getValue();
                    if(name.equals(instanceName) && password.equals(instancePassword)){
                        logIn = true;
                    }
                }
                if(logIn) {
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                }
                else{
                    Toast.makeText(LoginActivity.this,"Λάθος στοιχεία χρήστη", Toast.LENGTH_LONG).show();
                }


                System.err.println("name:  " + name);

                //finish();
            }
        });

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSkipped(true);
                Toast.makeText(LoginActivity.this,getString(R.string.welcome) + " !", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });

        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder mBuilder = new AlertDialog.Builder(LoginActivity.this);
                mBuilder.setTitle("Διεύθυνση email");
                final EditText emailAddress = new EditText(LoginActivity.this);

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                emailAddress.setLayoutParams(lp);
                mBuilder.setView(emailAddress);
                mBuilder.setCancelable(true);
                mBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                mBuilder.setNegativeButton("Ακυρο",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                return;
                            }
                        });
                final AlertDialog mDialog = mBuilder.create();
                mDialog.show();


                mDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(isEmailValid(emailAddress.getText().toString().trim())){
                            mDialog.dismiss();
                            Toast.makeText(LoginActivity.this, "Σας στείλαμε email με τα στοιχεία σας", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            emailAddress.setError(getString(R.string.invalid_email));
                        }
                    }
                });

            }
        });



        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                loginViewModel.loginDataChanged(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());
            }
        };
        usernameEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    loginViewModel.login(usernameEditText.getText().toString(),
                            passwordEditText.getText().toString());
                }
                return false;
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSkipped(false);
                loadingProgressBar.setVisibility(View.VISIBLE);
                loginViewModel.login(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());
            }
        });
    }

    private void updateUiWithUser(LoggedInUserView model) {
        //String welcome = getString(R.string.welcome) + model.getDisplayName();

        String welcome = getString(R.string.welcome) + name + " !";
        // TODO : initiate successful logged in experience
        Toast.makeText(getApplicationContext(), welcome, Toast.LENGTH_LONG).show();
    }

    private boolean isEmailValid(String email) {
        if (email == null || email.equals("")) {
            return false;
        }
        if (!email.contains("@")) {
            return false;
        } else {
            return true;
        }
    }

    /*private void showLoginFailed(@StringRes Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }*/
}