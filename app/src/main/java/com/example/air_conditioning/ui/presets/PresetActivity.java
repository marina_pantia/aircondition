package com.example.air_conditioning.ui.presets;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.example.air_conditioning.R;
import com.example.air_conditioning.ui.login.LoginActivity;
import com.example.air_conditioning.ui.main.ControlActivity;
import com.example.air_conditioning.ui.manual.AdvancedManual;
import com.example.air_conditioning.ui.manual.ControlManual;
import com.example.air_conditioning.ui.manual.PresetManual;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

public class PresetActivity extends AppCompatActivity {

    private static final String TAG = "PresetActivity";

    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> temperature = new ArrayList<>();
    private ArrayList<Integer> mImages = new ArrayList<>();
    private ArrayList<Integer> fanVolumeImages = new ArrayList<>();
    RecyclerView ourPrograms;
    RecyclerView yourPrograms;
    Button add;
    Button delete;
    Button next;

    static String device;

    private int image;
    private String name;
    private String tempr;
    public static HashMap<HashMap<String, String>, HashMap<String, Integer>> msg = new HashMap<>();
    public static HashMap<String, Integer> msgImage = new HashMap<>();
    public static HashMap<String, String> msgDeviceName = new HashMap<>();

    private ArrayList<PresetListItem> presetlist = new ArrayList<>();

    public static ArrayList<PresetListItem> yourPresets;
    //RecyclerViewAdapter adapter;

    private void Logout() {
        LoginActivity login = new LoginActivity();
        if (!login.getSkipped()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(PresetActivity.this);
            builder.setTitle("Μήνυμα επιβεβαίωσης");
            builder.setMessage("Είστε σίγουροι ότι θέλετε να αποσυνδεθείτε από τον λογαριασμό σας;");

            builder.setCancelable(true);
            builder.setPositiveButton("ΝΑΙ", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();
                    Intent intent = new Intent(PresetActivity.this, LoginActivity.class);
                    startActivity(intent);
                    return;

                }
            });
            builder.setNegativeButton("ΟΧΙ",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            return;
                        }
                    });
            builder.show();
        } else {
            Toast.makeText(PresetActivity.this, "Δεν έχετε συνδεθεί σε κάποιο λογαριασμό για να αποσυνδεθείτε", Toast.LENGTH_LONG).show();
        }
    }

    private void Manual() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(PresetActivity.this);
        builder.setTitle("Εγχειρίδιο");
        builder.setMessage("Ενότητες: ");

        final ListView unitList = new ListView(PresetActivity.this);

        ArrayList<String> listunit = new ArrayList<>();
        final String unitPresets = "Ενότητα 1η: Έτοιμα Προγραμμάτα";
        final String unitControl = "Ενότητα 2η: Τηλεχειριστήριο και εικονίδια";
        final String unitAdvanced = "Ενότητα 3η: Λειτουργία για προχωρημένους";

        listunit.add(unitPresets);
        listunit.add(unitControl);
        listunit.add(unitAdvanced);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(PresetActivity.this, android.R.layout.simple_list_item_1, android.R.id.text1, listunit);

        unitList.setAdapter(adapter);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        unitList.setLayoutParams(lp);
        builder.setView(unitList);
        builder.setCancelable(true);
        builder.setPositiveButton("ΟΚ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                return;

            }
        });

        builder.show();

        unitList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = (String) unitList.getItemAtPosition(position);
                if(item.equals(unitPresets)) {
                        Intent intent1 = new Intent(PresetActivity.this, PresetManual.class);
                        startActivity(intent1);
                }
                else if(item.equals(unitControl)) {
                        Intent intent2 = new Intent(PresetActivity.this, ControlManual.class);
                        startActivity(intent2);
                }
                else {
                    Intent intent3 = new Intent(PresetActivity.this, AdvancedManual.class);
                    startActivity(intent3);
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.manual: {
                Manual();
                System.err.println("Manual");
                break;
            }
            case R.id.logout: {
                Logout();
                break;
            }
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preset);
        ourPrograms = (RecyclerView) findViewById(R.id.rv_presets);
        yourPrograms = (RecyclerView) findViewById(R.id.rv_yourPresets);

        Log.d(TAG, "On Create: started.");

        Intent intent = getIntent();
        // DeviceListItem device_contents = (DeviceListItem) intent.getExtras().get("message_device");
        //device = device_contents.getName();
        device = (String) intent.getExtras().get("message_device");
        initImages();
        add = (Button) findViewById(R.id.btn_add);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PresetActivity.this, ControlActivity.class);
                PresetListItem presetItem = new PresetListItem(R.drawable.snowflake_50, R.drawable.fan_volume2_50, "Τυχαίο πρόγραμμα", "23°C");
                HashMap<String, PresetListItem> map = new HashMap<>();
                map.put(device, presetItem);
               /* msgDeviceName.put(device,"");
                msgImage.put("",0);
                msg.put(msgDeviceName,msgImage);
                intent.putExtra("message", msg);*/
                intent.putExtra("message", map);
                startActivity(intent);
            }
        });
    }

    private void initImages() {
        Log.d(TAG, "initImageBitmaps: preparing bitmaps.");

        mNames.add("Πρόγραμμα 1");
        temperature.add("23°C");
        mImages.add(R.drawable.snowflake_50);
        fanVolumeImages.add(R.drawable.fan_volume3_50);

        mNames.add("Πρόγραμμα 2");
        temperature.add("26°C");
        mImages.add(R.drawable.sun_50);
        fanVolumeImages.add(R.drawable.fan_volume2_50);

        mNames.add("Πρόγραμμα 3");
        temperature.add("24°C");
        mImages.add(R.drawable.rain_50);
        fanVolumeImages.add(R.drawable.fan_volume1_50);


        for (int i = 0; i < mNames.size(); i++) {
            PresetListItem item = new PresetListItem(mImages.get(i), fanVolumeImages.get(i), mNames.get(i), temperature.get(i));
            System.err.println("mode image code:  " + mImages.get(i));
            System.err.println("fan volume image code:  " + fanVolumeImages.get(i));
            presetlist.add(item);
        }
        initRecyclerView();
    }

    private void initRecyclerView() {
        Log.d(TAG, "initRecyclerView: init recyclerview.");


        final RecyclerViewAdapter adapter = new RecyclerViewAdapter(presetlist);
        ourPrograms.setAdapter(adapter);
        ourPrograms.setLayoutManager(new LinearLayoutManager(PresetActivity.this));

        yourPresets = loadPresets(false, presetlist.get(0));
        final RecyclerViewAdapter adapter2 = new RecyclerViewAdapter(yourPresets);
        yourPrograms.setAdapter(adapter2);
        yourPrograms.setLayoutManager(new LinearLayoutManager(PresetActivity.this));


        msgImage.put(tempr, image);
        msgDeviceName.put(device, name);
        msg.put(msgDeviceName, msgImage);
        final HashMap<String, PresetListItem> options = new HashMap<>();
        adapter.setOnItemClickListener(new RecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                options.put(device, presetlist.get(position));
                Intent intent = new Intent(PresetActivity.this, ControlActivity.class);
                intent.putExtra("message", options);
                startActivity(intent);
            }
        });

        final HashMap<String, PresetListItem> options2 = new HashMap<>();
        adapter2.setOnItemClickListener(new RecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(final int position) {
                options2.put(device, yourPresets.get(position));
                AlertDialog.Builder builder = new AlertDialog.Builder(PresetActivity.this);
                builder.setTitle("Διαχείριση προγράμματος");
                // builder.setMessage("Είστε σίγουροι ότι θέλετε να αποσυνδεθείτε από τον λογαριασμό σας;");

                builder.setCancelable(true);
                builder.setPositiveButton("ΣΥΝΕΧΕΙΑ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                        Intent intent = new Intent(PresetActivity.this, ControlActivity.class);
                        intent.putExtra("message", options2);
                        startActivity(intent);
                        return;

                    }
                });
                builder.setNegativeButton("ΔΙΑΓΡΑΦΗ",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                AlertDialog.Builder builder2 = new AlertDialog.Builder(PresetActivity.this);
                                builder2.setTitle("Διαγραφή προγράμματος");
                                builder2.setMessage("Είστε σίγουροι ότι θέλετε να διαγράψετε αυτό το πρόγραμμα από τη λίστα σας;");

                                builder2.setCancelable(true);
                                builder2.setPositiveButton("ΝΑΙ", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        removePreset(position);
                                        Intent intent = new Intent(PresetActivity.this, PresetActivity.class);
                                        intent.putExtra("message_device", device);
                                        startActivity(intent);
                                        return;

                                    }
                                });
                                builder2.setNegativeButton("ΟΧΙ",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();

                                                return;
                                            }
                                        });
                                builder2.show();
                                return;
                            }
                        });
                builder.show();
                /*Intent intent = new Intent(PresetActivity.this , ControlActivity.class);
                intent.putExtra("message",options2);
                startActivity(intent);*/
            }
        });


    }

    public void removePreset(int position_preset){
        System.out.println("inside remove");
        yourPresets.remove(position_preset);
        for(int i = 0 ; i< yourPresets.size(); i++){
            System.out.println("temprature: " + yourPresets.get(i).getTemperature());
        }
        FileOutputStream fos = null;
        try {

            fos = openFileOutput("presets.txt",MODE_APPEND);
            File file = new File(getFilesDir()+"/presets.txt");
            fos.close();
            boolean deleted = file.delete();
            System.err.println(deleted);

            fos = openFileOutput("presets.txt",MODE_APPEND);

            for(int i = 0; i < yourPresets.size(); i++ ) {

                 fos.write(String.valueOf(yourPresets.get(i).getImages()).getBytes());
                 fos.write("^&*)^#$1".getBytes());
                 fos.write(String.valueOf(yourPresets.get(i).getFanVolume()).getBytes());
                 fos.write("^&*)^#$2".getBytes());
                 fos.write(yourPresets.get(i).getmImageNames().getBytes());
                 fos.write("^&*)^#$3".getBytes());
                 fos.write((yourPresets.get(i).getTemperature() + "°C").getBytes());
                 fos.write("\n".getBytes());
            }
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public ArrayList<PresetListItem> loadPresets(boolean delete, PresetListItem preset) {
        FileInputStream fis = null;
        ArrayList<PresetListItem> presets = new ArrayList<>();
        try {

                fis = openFileInput("presets.txt");
                InputStreamReader isr = new InputStreamReader(fis);
                BufferedReader br = new BufferedReader(isr);
                String text;
                String imageMode = null;
                int mode = 0;
                String imageFan = null;
                int fanV = 0;
                String namePreset = null;
                String tempr = null;

                while ((text = br.readLine()) != null) {

                    int index = text.indexOf("^&*)^#$1");
                    imageMode = text.substring(0, index);

                    System.out.println("imageMode: " + imageMode);
                    mode = Integer.parseInt(imageMode);
                    int index2 = text.indexOf("^&*)^#$2");
                    if (imageMode != null && imageFan == null) {


                        imageFan = text.substring(index + 8, index2);
                        System.out.println("imageFan: " + imageFan);
                        fanV = Integer.parseInt(imageFan);
                    }
                    int index3 = text.indexOf("^&*)^#$3");
                    if (imageMode != null && imageFan != null && namePreset == null) {

                        namePreset = text.substring(index2 + 8, index3);
                        System.out.println("namePreset: " + namePreset);

                    }

                    if (imageMode != null && imageFan != null && namePreset != null && tempr == null) {
                        tempr = text.substring(index3 + 8, text.length());
                        System.out.println("tempr: " + tempr);
                        presets.add(new PresetListItem(mode, fanV, namePreset, tempr  ));
                        imageMode = null;
                        imageFan = null;
                        namePreset = null;
                        tempr = null;
                    }

                }
                fis.close();


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return presets;
        }
}