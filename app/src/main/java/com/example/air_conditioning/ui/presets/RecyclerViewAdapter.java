package com.example.air_conditioning.ui.presets;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.air_conditioning.R;
import com.example.air_conditioning.ui.devices.DeviceListAdapter;
import com.example.air_conditioning.ui.devices.DeviceListItem;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewAdapter extends  RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {


    private ArrayList<PresetListItem> presetlist;
    private RecyclerViewAdapter.OnItemClickListener mListener;


    @Override
    public Filter getFilter() {
        return null;
    }


    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    public void setOnItemClickListener(RecyclerViewAdapter.OnItemClickListener listener){
        mListener = listener;
    }


    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView mTextViewName;
        private TextView mTextViewTemperature;
        private ImageView mImageView;
        private ImageView fan_volume;
        List<View> itemViewList = new ArrayList<>();

        public ItemViewHolder(@NonNull final View itemView , final RecyclerViewAdapter.OnItemClickListener listener) {
            super(itemView);
            mTextViewName = itemView.findViewById(R.id.image_nameCold);
            mTextViewTemperature = itemView.findViewById(R.id.tv_temperature);
            mImageView = itemView.findViewById(R.id.imageCold);
            fan_volume = itemView.findViewById(R.id.fanVolume_iv);
            itemViewList.add(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }

        public void setItemData(PresetListItem item){
            mTextViewName.setText(item.getmImageNames());
            mTextViewTemperature.setText(item.getTemperature());
            mImageView.setImageResource(item.getImages());
            fan_volume.setImageResource(item.getFanVolume());
        }
    }


    public RecyclerViewAdapter(ArrayList<PresetListItem> itemList) {
        presetlist = itemList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewAdapter.ItemViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.layout_listitem, parent, false
                ),mListener
        );


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        PresetListItem item = (PresetListItem) presetlist.get(position);
        ((RecyclerViewAdapter.ItemViewHolder) holder).setItemData(item);


    }

    @Override
    public int getItemCount() {
        return presetlist.size();
    }


}
