package com.example.air_conditioning.ui.presets;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.air_conditioning.ui.devices.DeviceListItem;

import java.util.ArrayList;

public class PresetListItem implements Parcelable {

    private int mImages ;
    private int FanVolume;
    private String mImageNames ;
    private String temperature ;

    public PresetListItem(int mImages, int FanVolume,String mImageNames, String temperature){
        this.mImages = mImages;
        this.FanVolume = FanVolume;
        this.mImageNames =mImageNames;
        this.temperature = temperature;
    }


    protected PresetListItem(Parcel in) {
        this.mImages =  in.readInt();
        this.FanVolume = in.readInt();
        this.mImageNames = in.readString();
        this.temperature = in.readString();
    }

    public static final Creator<PresetListItem> CREATOR = new Creator<PresetListItem>() {
        @Override
        public PresetListItem createFromParcel(Parcel in) {
            return new PresetListItem(in);
        }

        @Override
        public PresetListItem[] newArray(int size) {
            return new PresetListItem[size];
        }
    };

    public String getTemperature() {
        return temperature;
    }

    public String getmImageNames() {
        return mImageNames;
    }

    public int getImages() {
        return mImages;
    }

    public int getFanVolume() {
        return FanVolume;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public void setmImages(int mImages) {
        this.mImages = mImages;
    }

    public void setFanVolume(int FanVolume) {
        this.FanVolume = FanVolume;
    }


    public void setmImageNames(String mImageNames) {
        this.mImageNames = mImageNames;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mImages);
        dest.writeInt(FanVolume);
        dest.writeString(mImageNames);
        dest.writeString(temperature);
    }
}
